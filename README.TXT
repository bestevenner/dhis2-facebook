RUN:
1. follow guide: https://wiki.uio.no/mn/ifi/inf5750/index.php/Webstorm_deploy (we are group 3)
2. install Bower. at school pc's you need to Install linuxbrew(or homebrew)
2.1 install npm via linuxbrew: 'brew install npm'
2.2 install bower via npm: 'npm install bower'
2.3 run bower install. (it runs the added json file)
3 run at dhis servers: http://inf5750-3.uio.no/apps/<your appname here>
(i saved my project as henrikej-dhis. we should probably save them as different apps while we test.)

Your program should now run at the dhis server
You can push it to the server automatically at tools -> deployment -> automatic upload.


If you have any problems running it through bower. You can still do it the old way.
Just uncomment the src that points to angular/json web pages.

1. Download webstorm
2. run http://localhost:63342/<Path to index.html> in browser
3. Be happy.




Setup facebook app:
1. Create a facebook app from developers.facebook.com
2. Change the appId in social_media.js
3. Change App Domain and Site Url in Basic Settings on developers.facebook.com for the app

Add to DHIS2 servers:
Make a zip with manifest.webapp in the outer folder.
upload to server. (may need to delete old app for it to work)

Setup angularJS in Webstorm from terminal:
node install
npm install -g bower
