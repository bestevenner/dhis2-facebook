/**
 * Created by esten on 10.11.14.
 */

'use strict';

angular.module('intApp').controller('CommentController', ['$scope','$http', '$timeout', function($scope, $http, $timeout){
    $scope.add_comment = function(comment, id){
        console.log("Posting comment");
        $http({
            url: "http://inf5750-3.uio.no/api/interpretations/" + id + "/comments",
            method: "POST",
            headers: {'Content-Type': 'text/plain'},
            data: comment
        }).success(function(data, status, headers, config) {
            console.log("Success");
            $timeout(function() {
                console.log("Timeout when posting comment");
                $scope.updateInterpretation(id);
            });
        }).error(function(data, status, headers, config) {
            console.log("Error");
        })
    };

    $scope.deleteComment = function(postid, commentid) {
        console.log("Deleting");
        $http({
            url: "http://inf5750-3.uio.no/api/interpretations/" + postid + "/comments/" + commentid,
            method: "DELETE"
        }).success(function(data, status, headers, config) {
            console.log("Deleted");
            $timeout(function() {
                console.log("Timeout when deleting comment");
                $scope.updateInterpretation(postid);
            });
        }).error(function(data, status, headers, config) {
            console.log("Error");
        })
    }
}]);
