var appId = '309258945937208';
var rootUrl = serverUrl + '/apps/estenhl-dhis2/index.html';
var header = 'DHIS Interpretation wall';

var twitter_consumer_key = "7lhdYFOq4KmsjVdRMU7XDC3s7";
var twitter_consumer_secret = "gI3uCTiZ5FdvgMUD4SCUWVNd3giYcYIuTwqdFfsnoZpw1dunJs";

function loginCallback() {
    console.log("Login callback");
}

// Initialize FB API
window.fbAsyncInit = function() {
    console.log("Initializing Facebook API");
    FB.init({
        appId      : appId,
        xfbml      : true,
        version    : 'v2.1'
    });
};

console.log("Done initializing FB");

var intApp = angular.module('intApp');

intApp.controller('MediaCtrl', ['$scope', function($scope) {
    $scope.share = function (post) {
        var pictureUrl = null;
        if (post.map != undefined && post.map.href != undefined) {
            pictureUrl = post.map.href;
        } else if (post.chart != undefined && post.chart.href != undefined) {
            pictureUrl = post.chart.href;
        } else if (post.reportTable != undefined && post.reportTable.href != undefined) {
            pictureUrl = post.reportTable.href;
        } else if (post.dataSetReport != undefined && post.dataSetReport.href != undefined) {
            pictureUrl = post.dataSetReport.href;
        } else {
            pictureUrl = post.dataSet.href;
        }
        pictureUrl += '/data.jpg';
        console.log("pictureUrl: " + pictureUrl);
        var linkUrl = rootUrl + '##' + post.id;
        if (post.id == "d3BukolfFZI") {
            console.log("Hardcoded image");
            pictureUrl = "http://i61.tinypic.com/hvp4x4.jpg";
        }

        FB.ui(
            {
                method: 'feed',
                name: header,
                link: linkUrl,
                picture: pictureUrl,
                caption: post.caption,
                description: post.text,
                message: ''
            });
    };

    $scope.tweet = function (status, id) {
        var url = rootUrl + '##' + id;
        var length = url.length;
        if (status.length + length > 140) {
            status = status.substring(0, 77);
            status += "...";
        }

        var text = status + '\n\n' + url;

        var twtLink = 'http://twitter.com/home?status=' + encodeURIComponent(text);
        window.open(twtLink, '_blank');
    };
}]);