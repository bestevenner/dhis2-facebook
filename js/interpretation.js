/**
 * Created by esten on 10.11.14.
 */
/** I created 2 static interpretations from the test data inside data directory.
 * I added the xml files just to have a look into how the interpretations should look.
 * that is the most relevant for the project in order to keep it simple for now :)
 */

var serverUrl = "inf5750-3.uio.no";

'use strict';

function getInterpretationsData(){
    var json = {
        "interpretations": [
            {
                "id": "MC8iaQ5ojv6",
                "created": "2014-04-16T09:07:06.528+0000",
                "name": "MC8iaQ5ojv6",
                "lastUpdated": "2014-04-16T09:08:07.241+0000",
                "interpretationText": "Data looks suspicious. Is expected preg too low, or is a data entry mistake?",
                "type": "chart",
                "chartId": "DkPKc1EUmC2",
                "chartName": "ANC: 1-3 trend lines last 12 months",
                "chartCreated": "2014-04-16T09:04:55.400+0000",
                "chartLastUpdated": "2014-04-16T12:51:17.369+0000",
                "userId": "GOLswS44mh8",
                "userName": "Tom Wakiki",
                "comments": [{"commentId": "AWY8sx6qlrJ"}]

            },
            {
                "id": "d3BukolfFZI",
                "created": "2013-10-07T11:37:19.273+0000",
                "name": "d3BukolfFZI",
                "lastUpdated": "2014-04-15T22:18:23.989+0000",
                "interpretationText": "We can see that the ANC 2 coverage of Kasonko and Lei districts are under 40 %. Interventions are needed. What could be the cause for this?",
                "type": "map",
                "mapId": "bhmHJ4ZCdCd",
                "mapName": "ANC: ANC 2 Coverage",
                "mapCreated": "2012-11-13T12:01:21.918+0000",
                "mapLastUpdated": "2012-11-13T12:01:21.918+0000",
                "userId": "xE7jOejl9FI",
                "userName": "John Traore",
                "comments": [{"commentId": "iB4Etq8yTE6"},{"commentId": "njbtbUtnQWR"}]
            }
        ]
    };




    return json;
} // end of function

// fetching data from server using angularJS
var intApp =angular.module('intApp', []);

intApp.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    console.log("Timeout");
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});

intApp.directive('imageonload', function() {
    console.log("Image loaded");
})

intApp.controller('ListCtrl', ['$scope', '$http', '$location', '$anchorScroll',
    function ($scope, $http, $location, $anchorScroll) {

        $scope.$on('ngRepeatFinished', function(finishedEvent) {
            $scope.scroll();
        })

        $scope.scroll = function() {
            $location.hash();
            $anchorScroll();
        }

        $http({method:'GET',
            url: 'http://inf5750-3.uio.no/api/interpretations.json'

        }).success(function(data) {
            // takes only interpretations
            var mydata = data.interpretations;
            $scope.interpretations = [];

            //reads details for every interpretation
            angular.forEach(mydata, function(todo){


                $http.get('http://inf5750-3.uio.no/api/interpretations/' + todo.id)
                    .then(function(result){
                        //deletes comments. wrong pointing link which is not useful.
                        delete result.data.comments;
                        //gets comments user for each interpretation
                        $http.get('http://inf5750-3.uio.no/api/interpretations/' + todo.id +'/comments')
                            .then(function(commentsresult){
                                //adds the right attribute comment to an interpretation
                                result.data['comments'] = commentsresult.data.comments;

                            });
                        //adds an interpretation to interpretations
                        $scope.interpretations = $scope.interpretations.concat(result.data)

                    });
            });
        });

        $scope.done = function() {
            $scope.scroll();
        }

        $scope.updateInterpretation = function(id) {
            var i;
            for (i = 0; i < $scope.interpretations.length; i++) {
                if ($scope.interpretations[i].id == id) {
                    var found = i;
                    $http.get('http://inf5750-3.uio.no/api/interpretations/' + id +'/comments')
                        .then(function(commentsresult) {
                            $scope.interpretations[found].comments = commentsresult.data.comments;
                            angular.element(document.querySelector('#' + id)).scope().$apply();
                            document.getElementById("commentArea" + id).placeholder = "Your comment";
                        });
                }
            }
        }
    }]);



